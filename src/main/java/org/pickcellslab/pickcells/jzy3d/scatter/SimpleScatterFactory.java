package org.pickcellslab.pickcells.jzy3d.scatter;

import org.pickcellslab.foundationj.annotations.CoreImpl;
import org.pickcellslab.foundationj.services.simplecharts.SimpleScatter3D;
import org.pickcellslab.foundationj.services.simplecharts.SimpleScatter3DFactory;

@CoreImpl
public class SimpleScatterFactory implements SimpleScatter3DFactory{

	@Override
	public SimpleScatter3D createChart() {
		return new SimpleScatter();
	}

}
