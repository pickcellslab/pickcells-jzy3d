package org.pickcellslab.pickcells.jzy3d.scatter;

import java.util.function.Function;

import org.jzy3d.colors.Color;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.pickcells.jzy3d.shared.ColorCode;

public class ColorManager {

	private final ColorCode code = new ColorCode();
	private Dimension<DataItem, ? extends Number> dim;

	//Appearance
	private static final Color[] cs = {new Color(0f, 0f, 0f, 0.8f), new Color(1f, 0f, 0f, 0.5f),
			new Color(0f, 0f, 1f, 0.5f),new Color(0f, 1f, 0f, 0.5f),new Color(1f, 0f, 1f, 0.5f),new Color(1f, 0f, 1f, 0.5f)};
	private int next = 0;

	public ColorManager(Dimension<DataItem, ? extends Number> dim) {
		this.dim = dim;
	}


	public Function<DataItem, ? extends Number> getFunction(){
		return dim;
	}
	
	public Color get(double value, double min, double max){
		return code.getColor(value, min, max);
	}


	public Color nextBaseColor() {
		return cs[next++ % cs.length];
	}


}
