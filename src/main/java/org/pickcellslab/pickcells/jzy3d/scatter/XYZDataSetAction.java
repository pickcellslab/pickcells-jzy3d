package org.pickcellslab.pickcells.jzy3d.scatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.Scatter;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.actions.Action;
import org.pickcellslab.foundationj.dbm.queries.actions.ActionMechanism;

public class XYZDataSetAction implements Action<DataItem, Scatter> {

	private ColorManager manager;
	private Dimension<DataItem, ? extends Number>[] dims;
	
	private final Map<Object, XYZMechanism> coords = new HashMap<>();
	

	
	

	public XYZDataSetAction(ColorManager manager, Dimension<DataItem, ? extends Number>[] dims) {
		this.dims = dims;
		this.manager = manager;
	}
	
	
	@Override
	public ActionMechanism<DataItem> createCoordinate(Object key) {
		if(null == manager.getFunction()){
			XYZMechanism m = new XYZMechanism();
			coords.put(key, m);
			return m;
		}
		else{
			XYZCMechanism m = new XYZCMechanism();
			coords.put(key, m);
			return m;
		}			
	}

	@Override
	public Set<Object> coordinates() {
		return coords.keySet();
	}

	@Override
	public Scatter createOutput(Object key) throws IllegalArgumentException {
		if(null == manager.getFunction()){
			XYZMechanism m = coords.get(key);
			return new Scatter(m.coordinates.toArray(new Coord3d[m.coordinates.size()]), manager.nextBaseColor());
		}
		else{
			XYZCMechanism m = (XYZCMechanism) coords.get(key);
			Color[] colors = new Color[m.cols.size()];
			double min = m.stats.getMin();
			double max = m.stats.getMax();
			for(int c = 0; c<m.cols.size(); c++){
				colors[c] = manager.get(m.cols.get(c), min, max);
			}			
			return new Scatter(m.coordinates.toArray(new Coord3d[m.coordinates.size()]), colors);
		}	
	}

	@Override
	public String description() {
		return "Populate a 3D scatter plot";
	}
	
	
	private class XYZMechanism implements ActionMechanism<DataItem>{

		
		final List<Coord3d> coordinates = new ArrayList<>();		
				
		@Override
		public void performAction(DataItem i) throws DataAccessException {
			
			Number n0 = dims[0].apply(i);	if(n0==null)return;
			Number n1 = dims[1].apply(i);	if(n1==null)return;
			Number n2 = dims[2].apply(i);	if(n2==null)return;
			
			coordinates.add(new Coord3d(n0.doubleValue(), n1.doubleValue(), n2.doubleValue()));
		}
		
	}
	
	
	private class XYZCMechanism extends XYZMechanism{
		
		final List<Double> cols = new ArrayList<>();
		final SummaryStatistics stats = new SummaryStatistics();
		
		
		@Override
		public void performAction(DataItem i) throws DataAccessException {
			
			Number n0 = dims[0].apply(i);	if(n0==null)return;
			Number n1 = dims[1].apply(i);	if(n1==null)return;
			Number n2 = dims[2].apply(i);	if(n2==null)return;
			
			
			Number c = manager.getFunction().apply(i);	if(c==null)return;
			double v = c.doubleValue();
			cols.add(v);
			stats.addValue(v);
			coordinates.add(new Coord3d(n0.doubleValue(), n1.doubleValue(), n2.doubleValue()));
			
		}
	}
	
	

}
