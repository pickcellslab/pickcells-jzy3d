package org.pickcellslab.pickcells.jzy3d.scatter;

import java.awt.Component;
import java.awt.Dimension;
import java.text.Format;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.jzy3d.chart.Chart;
import org.jzy3d.chart.controllers.thread.camera.CameraThreadController;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.chart.factories.IChartComponentFactory.Toolkit;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.AbstractDrawable;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.pickcellslab.foundationj.services.simplecharts.DefaultSimpleChartModel;
import org.pickcellslab.foundationj.services.simplecharts.SeriesChangeListener;
import org.pickcellslab.foundationj.services.simplecharts.SimpleChartModel;
import org.pickcellslab.foundationj.services.simplecharts.SimpleScatter3D;
import org.pickcellslab.foundationj.services.simplecharts.SimpleSeries;
import org.pickcellslab.pickcells.jzy3d.camera.ZoomingAWTController;

public class SimpleScatter implements SimpleScatter3D, SeriesChangeListener<double[]> {

	private final DefaultSimpleChartModel<double[]> model;
	private final Chart chart;
	private final Map<String,AbstractDrawable> drMap = new HashMap<>();
	private final ColorManager colors;

	public SimpleScatter() {
		
		
		this.colors = new ColorManager(null);

		this.model = new DefaultSimpleChartModel<>();

		// Dimensions updates
		model.addSeriesChangeListener(this);


		//Create the chart
		chart = AWTChartComponentFactory.chart(Quality.Advanced, Toolkit.awt);
		//chart.getView().setBackgroundColor(new Color(230,233,233));

		int size = 1;
		float x;
		float y;
		float z;
		float a;


		Coord3d[] points = new Coord3d[size];
		Color[]   colors = new Color[size];

		Random r = new Random();
		r.setSeed(0);

		for(int i=0; i<size; i++){
			x = r.nextFloat() - 0.5f;
			y = r.nextFloat() - 0.5f;
			z = r.nextFloat() - 0.5f;
			points[i] = new Coord3d(x, y, z);
			a = 0.25f;
			colors[i] = new Color(x, y, z, a);
		}

		Scatter scatter = new Scatter(points, colors);
		chart.getScene().add(scatter);



		//Add controllers
		ZoomingAWTController mouse   = new ZoomingAWTController(chart);
		chart.addController(mouse);
		//chart.addMouseListener(mouse);

		CameraThreadController thread = new CameraThreadController();
		//	mouse.addSlaveThreadController(thread);
		chart.addController(thread);

		((Component)chart.getCanvas()).setPreferredSize(new Dimension(400,400));


	}


	@Override
	public void setTitle(String title) {
		// TODO Auto-generated method stub

	}

	@Override
	public Component getScene() {
		return (Component)chart.getCanvas();
	}

	@Override
	public SimpleChartModel<double[]> getModel() {
		return model;
	}


	@Override
	public void seriesChanged(SeriesChange c, SimpleSeries<double[]> s) {
		
		if(SeriesChange.ADDED == c){		
			Color color = colors.nextBaseColor();
			Color[] cols = new Color[s.numValues()];
			Coord3d[] coords = new Coord3d[s.numValues()];
			for(int i = 0;i<s.numValues(); i++){
				double[] d = s.getValue(i);
				coords[i] = new Coord3d(d[0],d[1],d[2]);
				cols[i] = color;
			}			
			Scatter scatter = new Scatter(coords, cols);
			scatter.setWidth(4f);
			chart.getScene().getGraph().add(scatter);
			drMap.put(s.id(), scatter);
			//TODO color and legend
		}else{
			AbstractDrawable dr = drMap.get(s.id());
			chart.getScene().getGraph().remove(dr);
			System.out.println("removed -> "+s.id());
		}
	}
	
	@Override
	public void finalize() throws Throwable{
		chart.dispose();
		super.finalize();
	}


	@Override
	public void setAxisFormat(int axis, Format format) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void setAxisLabel(int axis, String label) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void showLegend(boolean b) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void setSeriesColor(int series, java.awt.Color color) {
		// TODO Auto-generated method stub
		
	}
	

}
