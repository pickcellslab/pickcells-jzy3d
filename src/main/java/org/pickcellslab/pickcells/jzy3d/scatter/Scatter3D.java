package org.pickcellslab.pickcells.jzy3d.scatter;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;
import java.util.logging.Level;

import org.jzy3d.chart.Chart;
import org.jzy3d.chart.controllers.mouse.camera.AWTCameraMouseController;
import org.jzy3d.chart.controllers.thread.camera.CameraThreadController;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.chart.factories.IChartComponentFactory.Toolkit;
import org.jzy3d.colors.Color;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.plot3d.primitives.AbstractDrawable;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dataviews.events.FittingEvent;
import org.pickcellslab.foundationj.dataviews.fitting.FitResult;
import org.pickcellslab.foundationj.dataviews.fitting.MultiVariateRealDistributionFitterFactory;
import org.pickcellslab.foundationj.dataviews.mvc.DataSet;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableBrokerContainer;
import org.pickcellslab.foundationj.dataviews.mvc.ScalarDimensionBroker;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.charts.NumericChartModel;
import org.pickcellslab.pickcells.api.app.charts.StandardNumericDBChart;

public class Scatter3D implements StandardNumericDBChart<double[], JZYAppearance>{

	private final NotificationFactory notifier;
	
	//Model
	private NumericChartModel<DataItem,double[]> model;

	//View
	private Chart chart;
	private final String[] axes = {"X", "Y", "Z"};

	//View State
	private String selectedQuery;
	@SuppressWarnings("unchecked")
	private Dimension<DataItem, Number>[] dims = new Dimension[3];

	private Dimension<DataItem, Number> colorDim;








	public Scatter3D(List<MultiVariateRealDistributionFitterFactory> fitters, NotificationFactory notifier) {

		this.notifier = notifier;

		//Create the default model
		final Predicate<Integer> p = i->i==3;
		model = new NumericChartModel<>(p, (List)fitters);

		//Register listeners	

		//DataSet updates
		model.addDataEvtListener(this);

		// Dimensions updates
		model.addBrokerChangedListener(this);


		//Create the chart
		chart = AWTChartComponentFactory.chart(Quality.Advanced, Toolkit.awt);
		//chart.getView().setBackgroundColor(new Color(230,233,233));

		int size = 1;
		float x;
		float y;
		float z;
		float a;
		
		
		Coord3d[] points = new Coord3d[size];
		Color[]   colors = new Color[size];

		Random r = new Random();
		r.setSeed(0);
		
		for(int i=0; i<size; i++){
			x = r.nextFloat() - 0.5f;
			y = r.nextFloat() - 0.5f;
			z = r.nextFloat() - 0.5f;
			points[i] = new Coord3d(x, y, z);
			a = 0.25f;
			colors[i] = new Color(x, y, z, a);
		}

		Scatter scatter = new Scatter(points, colors);
		chart.getScene().add(scatter);
		
		
		
		//Add controllers
		AWTCameraMouseController mouse   = new AWTCameraMouseController(chart);
		chart.addController(mouse);
		//chart.addMouseListener(mouse);

		CameraThreadController thread = new CameraThreadController();
	//	mouse.addSlaveThreadController(thread);
		chart.addController(thread);		

	}






	@Override
	public void setSeriesVisible(String query, int series, boolean visible) {
		// TODO Auto-generated method stub

	}

	@Override
	public JZYAppearance getSeriesAppearance(String query, int series) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSeriesAppearance(String query, int series, JZYAppearance appearance) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setModel(NumericChartModel<DataItem,double[]> model) {
		throw new RuntimeException("Not implemented");
	}




	@Override
	public NumericChartModel<DataItem,double[]> getModel() {
		return model;
	}



	@Override
	public void dataSetChanged(DataSet<DataItem> oldDataSet, DataSet<DataItem> newDataSet) {
		
		List<AbstractDrawable> list = new ArrayList<>(chart.getScene().getGraph().getAll());
		list.forEach(dr -> chart.getScene().getGraph().remove(dr));
		selectedQuery = newDataSet.queryID(0);
	}






	@Override
	public JZYAppearance getFitAppearance(FitResult<?> fit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFitAppearance(FitResult<?> fit, JZYAppearance appearance) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setFitVisible(FitResult<?> fit, boolean isVisible) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fittingUpdated(FittingEvent evt) {
		// TODO Auto-generated method stub

	}

	@Override
	public String getQueryUnderFocus() {
		return selectedQuery;
	}




	@Override
	public int numDimensionalAxes() {
		return 3;
	}

	@Override
	public String axisName(int index) {
		return axes[index];
	}

	@Override
	public Dimension<DataItem, Number> currentAxisDimension(int index) {
		return dims[index];
	}
	
	
	

	@Override
	public void brokerChanged(
			RedefinableBrokerContainer<DataItem, ScalarDimensionBroker<DataItem, double[]>, Number> source,
			String query, ScalarDimensionBroker<DataItem, double[]> old,
			ScalarDimensionBroker<DataItem, double[]> broker) {
	
		
		if(old != null){//remove previsously loaded data
			List<AbstractDrawable> list = new ArrayList<>(chart.getScene().getGraph().getAll());
			list.forEach(dr -> chart.getScene().getGraph().remove(dr));
			old.removeFittingListener(this);
		}

		//load the data
		if(broker != null){

			broker.addFittingListener(this);

			//Get the selected dimensions
			Dimension<DataItem,Number> d1 = broker.fittableDimension(0);
			Dimension<DataItem,Number> d2 = broker.fittableDimension(1);
			Dimension<DataItem,Number> d3 = broker.fittableDimension(2);

			dims[0] = d1; dims[1] = d2; dims[2] = d3;

			ColorManager manager = new ColorManager(colorDim);

			try {


				for(int q = 0; q<model.getDataSet().numQueries(); q++){		
					ResultAccess<String, Scatter> r = model.getDataSet().loadQuery(q, new XYZDataSetAction(manager, dims));
					for(int s = 0; s<r.size(); s++){
						Scatter scatter =  r.getResult(s);
						scatter.setWidth(4f);
						chart.getScene().getGraph().add(scatter);
					}
				}


			} catch (DataAccessException e) {
				notifier.display("Error", "An error occured while populating the 2D Scatter plot with the database content", e, Level.WARNING);
			}
		}







	}


	public void dispose(){
		chart.getCanvas().dispose();
		chart.dispose();
	}


	public Component getView() {
		return (Component)chart.getCanvas();
	}











}
