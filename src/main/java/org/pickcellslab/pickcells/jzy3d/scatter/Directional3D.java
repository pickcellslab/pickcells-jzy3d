package org.pickcellslab.pickcells.jzy3d.scatter;

import java.awt.Component;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.logging.Level;

import org.jzy3d.chart.Chart;
import org.jzy3d.chart.controllers.thread.camera.CameraThreadController;
import org.jzy3d.chart.factories.IChartComponentFactory.Toolkit;
import org.jzy3d.colors.Color;
import org.jzy3d.colors.ColorMapper;
import org.jzy3d.maths.Coord3d;
import org.jzy3d.maths.IntegerCoord2d;
import org.jzy3d.plot2d.primitive.ColorbarImageGenerator;
import org.jzy3d.plot3d.primitives.AbstractDrawable;
import org.jzy3d.plot3d.primitives.LineStrip;
import org.jzy3d.plot3d.primitives.Point;
import org.jzy3d.plot3d.primitives.Polygon;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.primitives.Shape;
import org.jzy3d.plot3d.primitives.axes.layout.IAxeLayout;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.jzy3d.plot3d.rendering.legends.colorbars.AWTColorbarLegend;
import org.jzy3d.plot3d.rendering.tooltips.TextTooltipRenderer;
import org.jzy3d.plot3d.rendering.view.AWTView;
import org.jzy3d.plot3d.text.align.Halign;
import org.jzy3d.plot3d.text.drawable.DrawableTextBitmap;
import org.pickcellslab.foundationj.datamodel.DataItem;
import org.pickcellslab.foundationj.datamodel.dimensions.Dimension;
import org.pickcellslab.foundationj.dataviews.events.FittingEvent;
import org.pickcellslab.foundationj.dataviews.events.Modif;
import org.pickcellslab.foundationj.dataviews.fitting.Distribution;
import org.pickcellslab.foundationj.dataviews.fitting.FitResult;
import org.pickcellslab.foundationj.dataviews.mvc.DataSet;
import org.pickcellslab.foundationj.dataviews.mvc.RedefinableBrokerContainer;
import org.pickcellslab.foundationj.dataviews.mvc.TransformableDimensionBroker;
import org.pickcellslab.foundationj.dataviews.mvc.VectorAction;
import org.pickcellslab.foundationj.dataviews.mvc.VectorDimensionBroker;
import org.pickcellslab.foundationj.dbm.access.DataAccessException;
import org.pickcellslab.foundationj.dbm.queries.ResultAccess;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.charts.DirectionalDBChart;
import org.pickcellslab.pickcells.api.app.charts.VectorChartModel;
import org.pickcellslab.pickcells.api.app.charts.VectorRotationFactory;
import org.pickcellslab.pickcells.api.app.directional.DirectionalRealMultiVariateDistributionFitterFactory;
import org.pickcellslab.pickcells.jzy3d.camera.ZoomingAWTController;
import org.pickcellslab.pickcells.jzy3d.shared.ColorCode;
import org.pickcellslab.pickcells.jzy3d.shared.ColorLegend;
import org.pickcellslab.pickcells.jzy3d.shared.IcoSphereCreator;
import org.pickcellslab.pickcells.jzy3d.shared.IcoSphereCreator.TriangleIndices;
import org.pickcellslab.pickcells.jzy3d.shared.PickCellsChartFactory;

public class Directional3D implements DirectionalDBChart<JZYAppearance>{

	private final NotificationFactory notifier;
	
	
	//Model
	private VectorChartModel<DataItem> model;

	//View
	private Chart chart;
	private final String[] axes = {"Vector"};

	//View State
	private String selectedQuery;

	private Dimension<DataItem, double[]> selected;

	private Dimension<DataItem, ? extends Number> colorDim;

	private final Map<String, AbstractDrawable> series = new HashMap<>();

	private Shape bgSphere;

	private Color transparent = new Color(1.0f,1.0f,1.0f,0.0f);








	public Directional3D(List<DirectionalRealMultiVariateDistributionFitterFactory> fitters, NotificationFactory notifier) {
		
		this.notifier = notifier;

		//Create the default model
		final Predicate<Integer> p = i->i==3;
		model = new VectorChartModel<>(p , (List)fitters, new VectorRotationFactory<>());

		//Register listeners	

		//DataSet updates
		model.addDataEvtListener(this);

		// Dimensions updates
		model.addBrokerChangedListener(this);


		//Create the chart

		chart = PickCellsChartFactory.createChart(Quality.Advanced, Toolkit.awt);
		chart.getView().setBackgroundColor(new Color(230,233,233));
		
		//TODO remove
		chart.getView().getCamera().setScreenGridDisplayed(true);	
	  	
		//Adding central Axes
		chart.getView().setAxeBoxDisplayed(false);

		LineStrip lsX = new LineStrip(new Point(new Coord3d(-1.5, 0, 0), Color.BLACK), new Point(new Coord3d(1.5, 0, 0), Color.BLACK));

		List<Point> arrowX = new ArrayList<>(3);
		arrowX.add(new Point(new Coord3d(1.4,0,0.05), Color.BLACK));
		arrowX.add(new Point(new Coord3d(1.5,0,0), Color.BLACK));
		arrowX.add(new Point(new Coord3d(1.4,0,-0.05), Color.BLACK));

		LineStrip headX = new LineStrip();
		headX.addAll(arrowX);


		LineStrip lsY = new LineStrip(new Point(new Coord3d(0, -1.5, 0), Color.RED), new Point(new Coord3d(0,1.5, 0), Color.RED));
		lsY.setDisplayed(true);

		List<Point> arrowY = new ArrayList<>(3);
		arrowY.add(new Point(new Coord3d(0.05,1.4,0), Color.RED));
		arrowY.add(new Point(new Coord3d(0,1.5,0), Color.RED));
		arrowY.add(new Point(new Coord3d(-0.05,1.4,0), Color.RED));

		LineStrip headY = new LineStrip();
		headY.addAll(arrowY);

		Color orange = new Color(255,165,0);

		LineStrip lsZ = new LineStrip(new Point(new Coord3d(0, 0, -1.5), orange), new Point(new Coord3d(0, 0, 1.5), orange));
		lsZ.setDisplayed(true);



		List<Point> arrowZ = new ArrayList<>(3);
		arrowZ.add(new Point(new Coord3d(0,0.05,1.4), orange));
		arrowZ.add(new Point(new Coord3d(0,0,1.5), orange));
		arrowZ.add(new Point(new Coord3d(0,-0.05,1.4), orange));

		LineStrip headZ = new LineStrip();
		headZ.addAll(arrowZ);


		chart.getScene().add(lsX);
		chart.getScene().add(headX);
		chart.getScene().add(lsY);
		chart.getScene().add(headY);
		chart.getScene().add(lsZ);
		chart.getScene().add(headZ);


		// Adding axes legend
		final DrawableTextBitmap tx = new DrawableTextBitmap("X", new Coord3d(1.6,0,0), Color.BLACK);
		tx.setHalign(Halign.RIGHT); 

		chart.getScene().getGraph().add( tx );

		final DrawableTextBitmap ty = new DrawableTextBitmap("Y", new Coord3d(0,1.6,0), Color.RED);
		ty.setHalign(Halign.RIGHT); 
		chart.getScene().getGraph().add( ty );

		final DrawableTextBitmap tz = new DrawableTextBitmap("Z", new Coord3d(0,0,1.6), orange);
		tz.setHalign(Halign.RIGHT); 
		chart.getScene().getGraph().add( tz );




		//Add a white sphere outline
		//Create an IcoSphere to obtain a spherical surface
		IcoSphereCreator creator = new IcoSphereCreator();
		List<Coord3d> coords = creator.createPoints(2);
		int sampling = coords.size();
		System.out.println("Coords size = "+sampling);
		double[] x1 = new double[sampling];
		double[] y1 = new double[sampling];
		double[] z1 = new double[sampling];
		for(int j = 0; j<coords.size(); j++){
			x1[j] = coords.get(j).x;
			y1[j] = coords.get(j).y;
			z1[j] = coords.get(j).z;
		}

		//get the densities to color the sphere   
		Color bg = new Color(1f,1f,1f,0.1f);


		//Translate this sphere into a jzy3d surface
		List<Polygon> polygons = new ArrayList<>();

		for(TriangleIndices tri : creator.getTriangles()){

			Polygon pol = new Polygon();
			Coord3d p1 = coords.get(tri.v1);      
			pol.add(new Point(p1, bg));

			Coord3d p2 = coords.get(tri.v2);
			pol.add(new Point(p2, bg));

			Coord3d p3 = coords.get(tri.v3);
			pol.add(new Point(p3, bg));

			polygons.add(pol);
		}



		bgSphere = new Shape(polygons);
		//
		bgSphere.setWireframeColor(new Color(0.5f,0.5f,0.5f,0.4f));
		bgSphere.setWireframeDisplayed(true);
		//surface.setWireframeColor(Color.BLACK);    

		chart.getScene().getGraph().add(bgSphere);




		//Finally, set the content of this panel


		ZoomingAWTController mouse   = new ZoomingAWTController(chart);
		chart.addController(mouse);
		//addMouseListener(mouse);


		CameraThreadController thread = new CameraThreadController();
		mouse.addSlaveThreadController(thread);
		chart.addController(thread);	

	}






	@Override
	public void setSeriesVisible(String query, int series, boolean visible) {
		// TODO Auto-generated method stub

	}

	@Override
	public JZYAppearance getSeriesAppearance(String query, int series) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSeriesAppearance(String query, int series, JZYAppearance appearance) {
		// TODO Auto-generated method stub

	}




	@Override
	public void setModel(VectorChartModel<DataItem> model) {
		throw new RuntimeException("Not implemented");
	}




	@Override
	public VectorChartModel<DataItem> getModel() {
		return model;
	}



	@Override
	public void dataSetChanged(DataSet<DataItem> oldDataSet, DataSet<DataItem> newDataSet) {
		clearSeries();
		selectedQuery = newDataSet.queryID(0);
	}






	@Override
	public JZYAppearance getFitAppearance(FitResult<?> fit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFitAppearance(FitResult<?> fit, JZYAppearance appearance) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setFitVisible(FitResult<?> fit, boolean isVisible) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fittingUpdated(FittingEvent evt) {
		// TODO Auto-generated method stub

		if(evt.getType() == Modif.ADDED){

			FitResult<? extends Distribution<double[]>> result = model.dimensionBroker(this.selectedQuery).getDistributionFit(evt.stringId());
			Distribution<double[]> distribution = result.getFit();			
			
			//Create the color mapper
			
			ColorCode code = new ColorCode();
			code.setAlpha(0.4f);
			
			
			List<AbstractDrawable> points = bgSphere.getDrawables();

		//	double[] d = new double[points.size()];
			for(int i = 0; i< points.size(); i++){
				//d[i] = distribution.density(new double[]{p.x,p.y,p.z});
				((Polygon)points.get(i)).getPoints()
				.forEach(p->{
					double d = distribution.density(new double[]{p.xyz.x,p.xyz.y,p.xyz.z});
					p.setColor(code.getColor(d));				
				});
			}
			

			//double min = Statistics.min(d);
			//double max = Statistics.max(d);

			
			
			CodeMapper mapper = new CodeMapper(code,0,1.1);
			
			
			
			System.out.println("Directional3D: About to set color map");
			
			
			//Add a colorbar
			IAxeLayout layout = chart.getView().getAxe().getLayout();

			SphereLegend legend = new SphereLegend(bgSphere,layout, mapper);
			legend.setMinimumSize(new org.jzy3d.maths.Dimension(100,600));
			bgSphere.setLegend(legend);
			bgSphere.setLegendDisplayed(true);


			//Create a tootltip
			((AWTView)chart.getView()).setTooltip(
					new TextTooltipRenderer(result.getDescription(), new IntegerCoord2d(16,10),new Coord3d()));

		}
		else{
			((AWTView)chart.getView()).clearTooltips();
			((AWTView)chart.getView()).addTooltip(new TextTooltipRenderer("Dimension : "+selected, new IntegerCoord2d(10,10),new Coord3d()));
			
			bgSphere.setLegend(null);
			bgSphere.setLegendDisplayed(false);
		}

	}





	@Override
	public String getQueryUnderFocus() {
		return selectedQuery;
	}




	@Override
	public int numDimensionalAxes() {
		return 1;
	}

	@Override
	public String axisName(int index) {
		return axes[index];
	}

	@Override
	public Dimension<DataItem, double[]> currentAxisDimension(int index) {
		return selected;
	}



	@Override
	public void brokerChanged(RedefinableBrokerContainer<DataItem, VectorDimensionBroker<DataItem>, double[]> source,
			String query, VectorDimensionBroker<DataItem> old, VectorDimensionBroker<DataItem> broker) {


		if(old != null){//remove previsously loaded data
			clearSeries();
			old.removeFittingListener(this);
			old.removeTransformChangeListener(this);
			((AWTView)chart.getView()).clearTooltips();
			//TODO clear colors of the fit if loaded
		}

		//load the data
		if(broker != null){
			broker.addFittingListener(this);
			broker.addTransformChangeListener(this);
			//Get the selected dimensions
			selected = broker.fittableDimension(0);
			clearSeries();
			loadData(selected);
			((AWTView)chart.getView()).addTooltip(new TextTooltipRenderer("Dimension : "+selected, new IntegerCoord2d(10,10),new Coord3d()));
		}

	}





	private void loadData(Dimension<DataItem,double[]> selected) {


		ColorManager manager = new ColorManager(colorDim);

		try {

			System.out.println("Loading dimension "+selected.name());

			for(int q = 0; q<model.getDataSet().numQueries(); q++){		
				ResultAccess<String, List<double[]>> r = model.getDataSet().loadQuery(q, new VectorAction<>(selected));
				for(int s = 0; s<r.size(); s++){
					List<double[]> list = r.getResult(s);
					Coord3d[] coords = new Coord3d[list.size()];
					for(int i = 0; i<list.size(); i++){
						double[] d = list.get(i);
						coords[i] = new Coord3d(d[0],d[1],d[2]);
					}
					final Color color = manager.nextBaseColor();
					final Scatter scatter =  new Scatter(coords, color);
					
					scatter.setWidth(2.5f);
					
					ColorLegend legend = new ColorLegend(scatter,r.getKey(s));
					legend.setMinimumSize(new org.jzy3d.maths.Dimension(200,70));
					scatter.setLegend(legend);
					scatter.setLegendDisplayed(true);
					chart.getScene().getGraph().add(scatter);
					series.put(r.getKey(s), scatter);
				}
			}


		} catch (DataAccessException e) {
			notifier.display("Error", "An error occured while populating the 3D directional plot with the database content", e, Level.WARNING);
		}

	}






	@Override
	public void transformChanged(TransformableDimensionBroker<DataItem,double[]> source) {
		clearSeries();
		selected = source.dimension(0);
		((AWTView)chart.getView()).clearTooltips();
		((AWTView)chart.getView()).addTooltip(new TextTooltipRenderer("Transformed Dimension : "+selected, new IntegerCoord2d(10,10),new Coord3d()));
		loadData(selected);
	}


	private void clearSeries(){
		series.values().forEach(d->chart.getScene().getGraph().remove(d));
		bgSphere.getDrawables().forEach(d->{
			((Polygon)d).getPoints()
			.forEach(p->{
				p.setColor(transparent);				
			});
		});
	}









	public void dispose(){
		chart.getCanvas().dispose();
		chart.dispose();
	}


	public Component getView() {
		return (Component)chart.getCanvas();
	}








	private class SphereLegend extends AWTColorbarLegend{


		private CodeMapper mapper;

		public SphereLegend(AbstractDrawable parent, IAxeLayout layout, CodeMapper mapper) {
			super(parent, layout);
			this.mapper = mapper;
		}


		@Override
		public BufferedImage toImage(int width, int height) {

			// setup generator
			ColorbarImageGenerator bar = new ColorbarImageGenerator(mapper, provider, renderer);
			if(foreground!=null)
				bar.setForegroundColor(foreground);
			else
				bar.setForegroundColor(Color.BLACK);
			if(background!=null){
				bar.setBackgroundColor(background);
				bar.setHasBackground(true);
			}
			else
				bar.setHasBackground(false);

			// render @ given dimensions
			return bar.toImage(Math.max(width-25,1), Math.max(height-25,1));        

		}


	}


	private class CodeMapper extends ColorMapper{

		private ColorCode code;

		CodeMapper(ColorCode code, double min, double max){
			this.code = code;
			this.min = min;
			this.max = max;
		}

		@Override
		public Color getColor(double v){   
			Color out = code.getColor(v	, min, max);

			if(factor!=null)
				out.mul(factor);
			return out;
		} 

	}









}
