package org.pickcellslab.pickcells.jzy3d.scatter;



import java.awt.BorderLayout;
import java.awt.Image;
import java.net.URL;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.directional.DirectionalRealMultiVariateDistributionFitterFactory;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;


@Module
public class Directional3DFactory implements DBViewFactory{ 

	private final NotificationFactory notifier;
	private final List<DirectionalRealMultiVariateDistributionFitterFactory> fitters;
	
	public Directional3DFactory(List<DirectionalRealMultiVariateDistributionFitterFactory> fitters, NotificationFactory notifier) {
		this.fitters = fitters;
		this.notifier = notifier;
	}
	
	
	@Override
	public String toString() {
		return "Directional Plot 3D";
	}




	@Override
	public String description() {
		return "Create a 3D Directional Plot";
	}



	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}







	@Override
	public String name() {
		return "Scatter Plot 3D";
	}





	@Override
	public UIDocument createView(DataAccess access, DBViewHelp helper) {

		//Create the scatter
		Directional3D scatter = new Directional3D(fitters, notifier);

		// Create Controllers
		JPanel toolBar = helper.newToolBar()		
				.addChangeDataSetButton(() -> QueryConfigs.newTargetedOnlyToVector("").setSynchronizeTarget(true).build(), scatter.getModel())
				.addChangeDimensionsButton(scatter)
				.addTransformDimensionControl(scatter, "Normalize Vectors to other Vectors")
				.addCameleonControl(scatter, null)
				.addDistributionFittingControl(scatter.getModel())
				.build();
		
		
		JPanel scene = new JPanel();
		scene.setLayout(new BorderLayout());
		scene.add(toolBar, BorderLayout.NORTH);
		scene.add(new JScrollPane(scatter.getView()), BorderLayout.CENTER);

		UIDocument doc = new DefaultUIDocument(scene, name(), icon());
		doc.addDocumentClosedListener(l->{
			scatter.dispose();
		});
		
		return doc;


	}




	@Override
	public Icon icon() {
		return new ImageIcon(new ImageIcon(getClass().getResource("/icons/Von_mises_fisher.png")).getImage().getScaledInstance(24,24,Image.SCALE_FAST));
	}




	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
