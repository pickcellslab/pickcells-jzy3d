package org.pickcellslab.pickcells.jzy3d.scatter;


import java.awt.BorderLayout;
import java.net.URL;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.pickcellslab.foundationj.annotations.Module;
import org.pickcellslab.foundationj.dataviews.fitting.MultiVariateRealDistributionFitterFactory;
import org.pickcellslab.foundationj.dbm.access.DataAccess;
import org.pickcellslab.foundationj.dbm.queries.config.QueryConfigs;
import org.pickcellslab.foundationj.services.NotificationFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewFactory;
import org.pickcellslab.pickcells.api.app.charts.DBViewHelp;
import org.pickcellslab.pickcells.api.app.modules.DefaultUIDocument;
import org.pickcellslab.pickcells.api.app.modules.UIDocument;


@Module
public class Scatter3DFactory implements DBViewFactory{ 

	private final NotificationFactory notifier;
	private final List<MultiVariateRealDistributionFitterFactory> fitters;




	public Scatter3DFactory(List<MultiVariateRealDistributionFitterFactory> fitters, NotificationFactory notifier) {
		this.fitters = fitters;
		this.notifier = notifier;
	}
	
	
	
	@Override
	public String toString() {
		return "Scatter 3D";
	}




	@Override
	public String description() {
		return "Create a 3D Scatter Plot";
	}



	@Override
	public ViewType getType() {
		return ViewType.Numerical;
	}







	@Override
	public String name() {
		return "Scatter Plot 3D";
	}




	@Override
	public UIDocument createView(DataAccess access, DBViewHelp helper) {

		//Create the scatter
		Scatter3D scatter = new Scatter3D(fitters, notifier);

		// Create Controllers
		JPanel toolBar = helper.newToolBar()		
				.addChangeDataSetButton(() -> QueryConfigs.newTargetedOnlyConfig("").setSynchronizeTarget(true).build(), scatter.getModel())
				.addChangeDimensionsButton(scatter)
				.addCameleonControl(scatter, null)
				.addDistributionFittingControl(scatter.getModel())
				.build();
		
		
		JPanel scene = new JPanel();
		scene.setLayout(new BorderLayout());
		scene.add(toolBar, BorderLayout.NORTH);
		scene.add(new JScrollPane(scatter.getView()), BorderLayout.CENTER);

		UIDocument doc = new DefaultUIDocument(scene, name(), icon());
		doc.addDocumentClosedListener(l->{
			scatter.dispose();
		});
		
		return doc;


	}




	@Override
	public Icon icon() {
		return new ImageIcon(getClass().getResource("/icons/scatter3d.png"));
	}




	@Override
	public String authors() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public String licence() {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public URL url() {
		// TODO Auto-generated method stub
		return null;
	}


}
