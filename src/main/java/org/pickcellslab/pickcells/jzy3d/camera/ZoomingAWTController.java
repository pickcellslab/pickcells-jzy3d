package org.pickcellslab.pickcells.jzy3d.camera;

import java.awt.event.MouseWheelEvent;

import org.jzy3d.chart.Chart;
import org.jzy3d.chart.controllers.ControllerType;
import org.jzy3d.chart.controllers.mouse.camera.AWTCameraMouseController;

public class ZoomingAWTController extends AWTCameraMouseController{

	
	
	
	public ZoomingAWTController(Chart chart) {
		super(chart);
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		stopThreadController();
		float factor = 1 + (e.getWheelRotation() / 20.0f);
		for(Chart c: targets){
            c.getView().zoomX(factor, false);
            c.getView().zoomY(factor, false);
            c.getView().zoomZ(factor, true);
		}
		fireControllerEvent(ControllerType.ZOOM, factor);
	}
	
	
}
