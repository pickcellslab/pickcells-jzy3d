package org.pickcellslab.pickcells.jzy3d.shared;

import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;

import org.jzy3d.colors.Color;
import org.jzy3d.events.DrawableChangedEvent;
import org.jzy3d.maths.Dimension;
import org.jzy3d.plot3d.primitives.Scatter;
import org.jzy3d.plot3d.rendering.legends.AWTLegend;
import org.jzy3d.plot3d.rendering.view.ViewportConfiguration;


public class ColorLegend extends AWTLegend {

	private String seriesName;
	private Dimension minimumDimension;

	public ColorLegend(Scatter parent, String seriesName) {
		super(parent);
		this.seriesName = seriesName;
		//TODO debug only
		this.setScreenGridDisplayed(true);
	}

	
	
	@Override
	public void drawableChanged(DrawableChangedEvent evt) {
		// TODO Auto-generated method stub

	}


	@Override
	public BufferedImage toImage(int width, int height) {
		
		//System.out.println("ColorLegend : "+width+" ; "+height);
		
		// Init image output
		BufferedImage image = new BufferedImage(Math.max(width,1), Math.max(height,1), BufferedImage.TYPE_INT_ARGB);
		Graphics2D graphic = image.createGraphics();

		int txtSize=20;
		graphic.setFont(new java.awt.Font("Arial",Font.BOLD,txtSize)); 

		/*
		// Draw background
		if(hasBackground){
			graphic.setColor(ColorAWT.toAWT(backgroundColor));
			graphic.fillRect(0, 0, width, height);
		}
		 */
		final int boxDim = txtSize;

		// Draw the colored box 
		Color c = ((Scatter)parent).getColor();		
		graphic.setColor(new java.awt.Color(c.r,c.g,c.b));
		graphic.fillRect(0, boxDim, boxDim, boxDim);
		graphic.setColor(java.awt.Color.black);
		graphic.drawRect(0, boxDim, boxDim, boxDim);

		// Draw the Text
		graphic.drawString(seriesName, boxDim+5, boxDim*2);


		return image;

	}

	
	@Override
    public void render(GL gl, GLU glu){	
		//gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT); 
        gl.glEnable(GL2.GL_BLEND);
		super.render(gl, glu);
	}

	
	@Override
	public void setViewPort(ViewportConfiguration viewport) {
        super.setViewPort(viewport);
        
        if (imageWidth != viewport.getWidth() || imageHeight != viewport.getHeight())
            setImage(toImage(viewport.getWidth(), minimumDimension.height));
        
    }


	@Override
    public Dimension getMinimumSize(){
		return minimumDimension;
	}
	
	
    public void setMinimumSize(Dimension dimension){
		minimumDimension = dimension;
	}
   
    

}