package org.pickcellslab.pickcells.jzy3d.shared;

import java.util.List;

import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

import org.jzy3d.chart.Chart;
import org.jzy3d.chart.factories.AWTChartComponentFactory;
import org.jzy3d.plot3d.rendering.canvas.ICanvas;
import org.jzy3d.plot3d.rendering.canvas.Quality;
import org.jzy3d.plot3d.rendering.legends.ILegend;
import org.jzy3d.plot3d.rendering.scene.Scene;
import org.jzy3d.plot3d.rendering.view.ViewportBuilder;
import org.jzy3d.plot3d.rendering.view.ViewportConfiguration;
import org.jzy3d.plot3d.rendering.view.ViewportMode;
import org.jzy3d.plot3d.rendering.view.layout.ColorbarViewportLayout;
import org.jzy3d.plot3d.rendering.view.layout.IViewportLayout;

public class PickCellsChartFactory extends AWTChartComponentFactory{

	
	public static Chart createChart(Quality quality, Toolkit toolkit) {		
		return new Chart(new PickCellsChartFactory(), quality, toolkit.toString());
	}

	@Override
	public IViewportLayout newViewportLayout() {
		System.out.println("PickCells Vertical Layout In use!");
		return new ColumnLegendsViewportLayout();
	}



	private class ColumnLegendsViewportLayout extends ColorbarViewportLayout{

		private int minWidth;
		private int minHeight;

		@Override
		public void update(Chart chart) {
			final Scene scene = chart.getScene();
			final ICanvas canvas = chart.getCanvas();
			final List<ILegend> list = scene.getGraph().getLegends();

			// Compute an optimal layout so that we use the minimal area for metadata
			hasMeta = list.size() > 0;
			if (hasMeta) {
				minWidth = 0;
				minHeight = 0;
				for (ILegend data : list) {
					minWidth = Math.max(minWidth , data.getMinimumSize().width);
					minHeight = Math.max(minHeight , data.getMinimumSize().height);
				}
				screenSeparator = ((float) (canvas.getRendererWidth() - minWidth)) / ((float) canvas.getRendererWidth());///0.7f;
			}
			else{
				screenSeparator = 1.0f;
			}

			sceneViewPort = ViewportBuilder.column(canvas, 0, screenSeparator);
			backgroundViewPort = new ViewportConfiguration(canvas);
		}





		/**
		 * Renders the legend within the screen slice given by the left and right parameters.
		 */
		@Override
		protected void renderLegends(GL gl, GLU glu, float left, float right, List<ILegend> data, ICanvas canvas) {
			//System.out.println("PickCells Layout rendering legends");
			//float slice = (right - left) / (float) data.size();
			int k = 0;
			int x = (int) (canvas.getRendererWidth()*(screenSeparator));
			int width = (int) (canvas.getRendererWidth() - screenSeparator);
			for (ILegend layer : data) {
				layer.setViewportMode(ViewportMode.RECTANGLE_NO_STRETCH);
				//Setting y has no effect and legend is always centered in the height of the viewport (see AbstractViewport and Viewport.Mode)
				// hack by decreasing the size of the given viewport for each legend
				layer.setViewPort(new ViewportConfiguration(width, canvas.getRendererHeight()-(minHeight * ++k), x, canvas.getRendererHeight()));
				//layer.setViewPort(canvas.getRendererWidth(), canvas.getRendererHeight(), left + slice * (k++), left + slice * k);
				layer.render(gl, glu);
			}
			
		}




	}


}
